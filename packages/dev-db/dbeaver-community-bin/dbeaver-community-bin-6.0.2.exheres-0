# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require gtk-icon-cache freedesktop-desktop

# Just simply installs prebuilt version for now

MY_PN="dbeaver-ce"
SUMMARY="Free Universal SQL Client"
DESCRIPTION="Free multi-platform database tool for developers, SQL programmers,
database administrators and analysts. It supports all popular databases: MySQL, PostgreSQL,
MariaDB, SQLite, Oracle, DB2, SQL Server, Sybase, MS Access, Teradata, Firebird, Derby, etc.
"
HOMEPAGE="https://dbeaver.jkiss.org/"
DOWNLOADS="${HOMEPAGE}/files/${PV}/${MY_PN}-${PV}-linux.gtk.x86_64.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        dev-libs/glib:2
        virtual/jre:*
    recommendation:
        dev-libs/libsecret:1
"

WORK="${WORKBASE}/${PN%%-*}"

pkg_setup() {
    exdirectory --allow /opt
}

src_install() {
    local dest="/opt/${PN}"

    insinto "${dest}"
    doins -r ./*

    edo chmod +x "${IMAGE}/${dest}/dbeaver"
    dodir "/usr/$(exhost --target)/bin"
    dosym "${dest}/dbeaver" "/usr/$(exhost --target)/bin/dbeaver"

    insinto "/usr/share/applications"
    doins "${FILES}/dbeaver-community.desktop"
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

